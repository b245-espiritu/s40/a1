
const express = require('express');
const mongoose = require('mongoose');

// by default our backend's CORS setting will prevent any
// application outside our Express JS app to process the
// request. Using the cors package, it will allow us to 
// manipulate this and control what applications may use
// to our app

// It allows our backend application to be available in our frontend application

// Allows us to control the app's Cross Origin Resource Sharing
const cors = require('cors');

const userRoutes = require('./Routes/userRoutes.js')
const courseRoutes = require('./Routes/courRoutes.js');

const port = 3001;
const app = express();



  mongoose.set('strictQuery', true);
  mongoose.connect('mongodb+srv://admin:admin@batch245-espiritu.dgm8gby.mongodb.net/batch245_Course_API_Espiritu?retryWrites=true&w=majority',
        {
            useNewUrlParser:true,
            useUnifiedTopology: true
        }
    );

    let db = mongoose.connection;

    // erro handling in connecting
    db.on("error", console.error.bind(console, "Connection error"));

    // This will be triggered if the connection is succesfull
    db.once("open", ()=> console.log("we're connected to the cloud!"));




//Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());




app.use("/user", userRoutes);
app.use("/course", courseRoutes);

// 
app.listen(port, ()=> console.log(`Server is runnong at port: ${port}`))