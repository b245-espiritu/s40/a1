const jwt = require('jsonwebtoken')
/*
    - User defined string data that will be used to create JSON webtokens

    - used in the algo for encrypting our data which makes
    it difficult to decode the information without defined
    secret keyword.
*/


const secret = "CourseBookingAPI";

// [Section] JSON web token
 /**
    JSON web token or jwt is a way of securely
    passing the server to the frontend or the other
    parts of the server

    Information is kept secure through the use 
    of the secret code

    only the system will know the secret code
    that can decode the encrypted information
  */

// Token creation
 /**
    Analogy:
        pack the gift / information and provide the
        secret code for the key.
  */


        /*
             The argument that will be passed to our
            parameter(user) will be the document/clientInformation
            of our user.
        */
module.exports.createAccessToken = (user)=>{



    // serve as payload
    // will contain the data that will be passed to other parts of our API
    const data = {
        _id:user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }


    // .sign() - from jwt package will generate a JSON web token
    /**
        syntax: 
        jwt.sign(payload, secretCode, options)
     */
    return jwt.sign(data, secret, {})
}




// Token Verification
/*
    Analogy

    - Received the gift and open the lock to verify if
    the sender is legitimate and the gift was not tempered.
*/

// Middleware functions have access with request object and 
// response object, and the next function indicates that we
// may proceed with the next step.


module.exports.verify = (request, response, next) =>{
    // The token is retrived from the request headers
    // This can be provided in postman under
    // Authorization > bearer Token
    
    let token = request.headers.authorization;
    
    //Token recieved and is not undefined.
    console.log(token);
    
    if(typeof token !== "undefined"){
    // Retrives only token and removes the "Bearer" prefix
        token = token.slice(7,token.length )
        console.log(token);
    
        return jwt.verify(token, secret, (error, data)=>{
            if(error){
                return response.send({auth: "Failed"})
            }
            else{

                /*
                The verify method will be used as middleware in
                the route to verify the token before proceeding
                to the function that invokes the controller function.
                */
                next()
            }
        })
    
    }

    // token does not exist
    else{
        return response.send({auth: "Failed"})
    }
    
    }


// Token decryption

/*
    Analogy
    - Open the gift and get the content.
*/

module.exports.decode = (token)=>{
    if (typeof token !== "undefined"){
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data)=>{
            if(error){
                return null;
            }
            else{
                // decode method is used to obtain information from JWT.
                /**Syntax
                 * jwt.decode(token, [options])
                 */


                // return an object with access to the "payload"
                // property.
                return jwt.decode(token, {complete:true}).payload;
            }
        })
    }
    else{
        return null;
    }
}